from django.shortcuts import render
from django.core import serializers
from django.http import JsonResponse
from django.shortcuts import render
from .forms import UploadFileForm
from .models import UploadedFiles
from django.views.decorators.csrf import csrf_exempt
import json
import os
import requests

# Create your views here.
@csrf_exempt
def upload(request):
    os.system("rm -rf files")
    UploadedFiles.objects.all().delete()
    response = {}
    if request.method == 'POST':
        try:
            checkAuth = ambil_resource(request.headers['Authorization'])
            if checkAuth == 200:
                print("masuk")
                form = UploadFileForm(request.POST, request.FILES)
                print(form)
                if form.is_valid():
                    # file is saved
                    form.save()
                    files = serializers.serialize('json', UploadedFiles.objects.all())
                    uploads = json.loads(files)
                    url = 'http://' + request.get_host() + '/files/'
                    for upload in uploads:
                        name = upload['fields']['thefile']
                        title = upload['fields']['title']
                    url += name
                    response = {
                        'Title': title,
                        'Download' : url
                    }
            else:
                response = {
                    'detail' : 'Authentication token expired'
                }
        except KeyError as e:
            response = {
                    'detail' : 'Authentication credentials were not provided.'
            }
        
           
    return JsonResponse(response, safe=False)

def ambil_resource(access_token):
    URL = "http://oauth.infralabs.cs.ui.ac.id/oauth/resource"
    data_headers = { 'Authorization': 'Bearer ' + access_token }
    r = requests.get(url = URL, headers = data_headers)
    return r.status_code