from django.apps import AppConfig


class LinkDownloadConfig(AppConfig):
    name = 'link_download'
