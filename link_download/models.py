from django.db import models

# Create your models here.
# Create your models here.
class UploadedFiles(models.Model):
    title = models.CharField(max_length=50)
    thefile = models.FileField()

    def __str__(self):
        return self.title
    
    def delete(self, *args, **kwargs):
        self.thefile.delete()
        super().delete(*args, **kwargs)